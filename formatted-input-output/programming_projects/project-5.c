#include <stddef.h>
#include <stdio.h>

#define ROWS 4
#define COLS 4
#define DIAG_COUNT 2

int main(void) {
   int matrix[ROWS][COLS] = {0};
   int row_count[ROWS] = {0},
       column_count[COLS] = {0},
       diagonal_count[DIAG_COUNT] = {0};

   printf("Enter the numbers from 1 to 16 in any order:\n");
   for (size_t i = 0; i < ROWS; ++i) {
       for (size_t j = 0; j < COLS; ++j) {
           scanf("%d", &matrix[i][j]);
       }
   }
   printf("\n");

   for (size_t i = 0; i < ROWS; ++i) {
       for (size_t j = 0; j < COLS; ++j) {
           printf("%2d ", matrix[i][j]);
           row_count[i] += matrix[i][j];
           column_count[j] += matrix[i][j];

           if (i == j) {
               diagonal_count[0] += matrix[i][j];
           }

           if (i == ROWS - (j + 1)) {
               diagonal_count[1] += matrix[i][j];
           }
       }
       printf("\n");
   }
   printf("\n");

   printf("Row sums: ");
   for (size_t i = 0; i < ROWS; ++i) {
       printf("%d ", row_count[i]);
   }
   printf("\n");

   printf("Column sums: ");
   for (size_t i = 0; i < COLS; ++i) {
       printf("%d ", column_count[i]);
   }
   printf("\n");

   printf("Diagonal sums: ");
   for (size_t i = 0; i < DIAG_COUNT; ++i) {
       printf("%d ", diagonal_count[i]);
   }
   printf("\n");

    return 0;
}
