# Exercises

1. The outputs are:

```
    86, 1040
^^^^
 3.02530e+01
^
83.1620

1e-06
```

`^` represents whitespace.

2. See [`answer-2.c`](./answer-2.c)

3.
    a. Both "%d" and " %d" are equivalent.

    b. Both "%d-%d-%d" and "%d -%d -%d" are equivalent.

    c. "%f" and "%f " are not equivalent, because the latter has a trailing space, which means that it will keep waiting for input until it receives another non-whitespace character.

    d. Both "%f,%f" and "%f, %f" are equivalent.

4. When `10.3 5 6` is set as input, we get `10 0.3 5`

5. When `12.3 45.6 789` is set as input, we get `12.3 45 0.6`

6. See [`answer-6.c`](./answer-6.c)
