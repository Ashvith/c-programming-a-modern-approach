#include <stdio.h>
#include <math.h>

#define PI 3.14159265359

int main(void) {
    int radius;

    scanf("%d", &radius);
    float volume = PI * (4.0 / 3.0) * pow(radius, 3);

    printf("Volume: %g\n", volume);

    return 0;
}
