#include <stdio.h>
#include <math.h>

int main(void) {
    float x;

    printf("Enter the value of x: ");
    scanf("%f", &x);

    float result = (3 * pow(x, 5))
                    + (2 * pow(x, 4))
                    - (5 * pow(x, 3))
                    - pow(x, 2) + (7 * x) - 6;
    printf("Value is: %f\n ", result);

    return 0;
}
