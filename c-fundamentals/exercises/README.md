# Exercises

1. No, there is no warning message from the compiler.

2.
    a. Directive is `#include <stdio.h>`. Statements are:
      * `printf("Parkinson's Law:\nWork expands so as to ");`
      * `printf("fill the time\n");`
      * `printf("available for its completion.\n");`
      * `return 0;`

    b. The output of the program is:

      > Parkinson's Law:
      >
      > Work expands so as to fill the time
      >
      > available for its completion.

3. See [`answer-3.c`](./answer-3.c)

4. The answers are all initialized to zero when using `gcc` with optimization flag. Removing the optimization flag removes this behaviour. Similarly, using `clang` instead of `gcc` also follows a similar behavior.

5. (a) is not a legal C identifier, as variables cannot start with a number.

6. Having more than one adjacent underscores may lead to a spellign mistake.

7. (a) and (e) are keywords in C

8. There are 14 tokens in the given statement.
|Token|Count|
|-|-|
|answer|1|
|=|2|
|(|3|
|3|4|
|*|5|
|q|6|
|-|7|
|p|8|
|*|9|
|p|10|
|)|11|
|/|12|
|3|13|
|;|14|

9. After inserting spaces, this is how our statement looks like:
```c
answer = (3 * q - p * p) / 3;
```

10. The following spaces are important in [`dweight.c`](../dweight.c)

```c
#include <stdio.h>
        ^
int main(void) {
   ^
    int height, length, width, volume, weight;
       ^
    height = 8;
    length = 12;
    width = 10;
    volume = height * length * width;
    weight = (volume + 165) / 166;

    printf("Dimensions: %dx%dx%d\n", length, width, height);
    printf("Volume (cubic inches) : %d\n", volume);
    printf("Dimensional weight (pounds) : %d\n", weight);

    return 0;
          ^
}
```
